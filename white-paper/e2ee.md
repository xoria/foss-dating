# Communication end-to-end encryption (E2EE)

For the E2EE communication, we decided to use the
[libsignal protocol javascript library](https://github.com/WhisperSystems/libsignal-protocol-javascript).
