# Data protection & privacy

One of the main goals of this service is to hold data protection and privacy
values high.

Still, we don't want to enforce all its aspects on our users. For example, we
believe that due to data privacy, one should not be able to view any other users
whole profile, just like that. Especially the user pictures may have a high
privacy value to the owner and should be protected as well as possible and
desired by its owner. So we need to provide the user with the option to whom
certain sections of its profile should be visible and to whom not.

We attempt to solve this problem by profile stages. Every user can configure
each aspect of its profile to only be visible at a certain stage. While the
search takes all stages into account, the user can choose which stages are
visible to whom (including to unknown users during discovery). In addition, it
may choose to expose the fact whether there is some information regarding some
category available on some unlocked stage (without exposing its content).

All in all this enables people to highly customize their profile according to
their own privacy values, while it does not force onto them how they should
value their privacy.

## Profile storage

Regarding the storage of user profiles, we had to decide between different
methods. First, the classic model of storing the profile server-side could be
used. Its flaw is that the server has access to all profiles, which is a problem
in regard to data security.
In case the server gets compromised, all user profiles could leak. In contrast,
when saving the data purely on the user devices, there would be a high upload
traffic which would have negative effects on device battery and data plans.
Additionally, it would cause user profiles to only be discoverable when the user
is online and would highly complicate the user discovery algorithm (the server
would have to ask each user, if its profile matches to a single search query).

As a compromise, the profile data might be uploaded at the start of each
connection and cached in-memory by the server, dropping any references on a
disconnect.
This would reduce the traffic overhead, but not omit it all together. So, in
order to omit the traffic overhead, we decided to generate an encryption key
on the user client and store the encrypted profile data on the server --
persisted, not in memory. Now, instead of sending the whole profile on a new
connection, only the decryption key needs to be sent and kept in memory of the
server during the session.
This still results in profiles only being discoverable (and leak-able) while
their user is online. We think that this is a viable compromise. If it proves
not to be viable, we may add an option to store data unencrypted on the server
in the future.
