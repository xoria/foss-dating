const purgeCSS = require("@fullhuman/postcss-purgecss");
const { noop } = require("ts-essentials");

module.exports = {
  publicPath: process.env.BASE_URL || "/",

  css: {
    loaderOptions: {
      postcss: {
        postcssOptions: {
          plugins: [
            process.env.NODE_ENV === "production"
              ? purgeCSS({
                  content: [`./public/**/*.html`, `./src/**/*.vue`],
                  defaultExtractor(content) {
                    const contentWithoutStyleBlocks = content.replace(
                      /<style[^]+?<\/style>/gi,
                      ""
                    );
                    return [
                      ...(contentWithoutStyleBlocks.match(
                        /[A-Za-z0-9-_/:]*[A-Za-z0-9-_/]+/g
                      ) || []),
                      ...(
                        contentWithoutStyleBlocks.match(/:[A-Za-z0-9-_/]+/g) ||
                        []
                      ).map((it) => it.substring(1)),
                    ];
                  },
                  safelist: [
                    /-(leave|enter|appear)(|-(to|from|active))$/,
                    /^(?!(|.*?:)cursor-move).+-move$/,
                    /^router-link(|-exact)-active$/,
                    /data-v-.*/,
                    /data-e-.*/,
                  ],
                })
              : // prevent warning for no postcss plugin in development mode
                noop,
          ],
        },
      },
    },
  },

  pluginOptions: {
    prerenderSpa: {
      // registry: undefined,
      // renderRoutes: ["/", "/legal/imprint", "/legal/data-protection"],
      // useRenderEvent: true,
      // headless: true,
      // onlyProduction: true,
      // customRendererConfig: {
      //   // for better debugging (e.g. we had one global TypeError that prevented
      //   // further script execution, thus no render event and unlimited waiting)
      //   dumpio: true,
      // },
    },
  },
};
