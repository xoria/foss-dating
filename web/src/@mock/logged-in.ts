import {
  ProfileForeignSlim,
  ProfilePreviewDTO,
  ProfilePreviewLayout,
  ProfileSelf,
} from "@/modules/profile/profile.service";
import { LoggedInUserDTO, Role } from "@/modules/user/user.types";
import { Locale, LOCALES } from "@/i18n/constants";
import { delay } from "@/utils/promise";
import {
  genArray,
  genFakeUUID,
  genInt,
  genMessage,
  genName,
  genSwitch,
  sampleOne,
} from "@/@mock/mock.utils";
import { LikeDTO } from "@/modules/likes/likes.service";
import { addDays, eraseTimeOf } from "@/utils/datetime";
import {
  ChatItemDTO,
  ChatItemType,
  ChatMessageIncomingDTO,
  ChatMessageOutgoingDTO,
  ChatRoom,
  ChatRoomDTO,
} from "@/modules/chat/chat.service";
import { LocalizedSingle } from "@/i18n/provider";

const MAX_DELAY = 1000;
const MAX_DELAY_IDB = 200;
const HOUR_IN_MS = 1000 * 3600;

const USER: LoggedInUserDTO = {
  loggedIn: true,
  roles: [Role.USER],
  profiles: [
    { id: genFakeUUID(), label: "Relationship" },
    { id: genFakeUUID(), label: "Friends" },
    { id: genFakeUUID(), label: "Pen&Paper" },
  ],
};

let cDateLatestAction = Date.now() - genInt(HOUR_IN_MS * 48);
const CHAT_ROOMS = genArray(genInt(6)).map(
  (): ChatRoomDTO & {
    items: ChatItemDTO[];
  } => {
    let cDate = cDateLatestAction;
    const items: ChatItemDTO[] = genArray(genInt(12)).map(() => {
      const body = { locale: sampleOne(LOCALES)!, content: genMessage() };
      const item: ChatItemDTO = {
        id: genFakeUUID(),
        cDate,
        bodyHTML: body,
        ...genSwitch<
          | Pick<ChatMessageIncomingDTO, "type">
          | Pick<ChatMessageOutgoingDTO, "type" | "bodyOriginal">
        >(
          (): Pick<ChatMessageIncomingDTO, "type"> => ({
            type: ChatItemType.MSG_INCOMING,
          }),
          (): Pick<ChatMessageOutgoingDTO, "type" | "bodyOriginal"> => ({
            type: ChatItemType.MSG_OUTGOING,
            bodyOriginal: { ...body, content: `${body.content} [ original ]` },
          })
        ),
      };
      cDate -= genInt(HOUR_IN_MS * 4);
      return item;
    });
    cDateLatestAction -= genInt(HOUR_IN_MS * 24);
    items.reverse();
    return {
      id: genFakeUUID(),
      profileSelf: sampleOne(USER.profiles)!,
      profileForeign: genProfileForeign(),
      latestAction: items[items.length - 1],
      items,
    };
  }
);

export async function getMockUser(): Promise<LoggedInUserDTO> {
  await delay(Math.random() * MAX_DELAY);
  return USER;
}

export async function getMockChatRooms(): Promise<ChatRoomDTO[]> {
  await delay(Math.random() * MAX_DELAY_IDB);
  return CHAT_ROOMS.map((chat) => {
    const clone = { ...chat };
    Reflect.deleteProperty(clone, "items");
    return clone;
  });
}

export async function getMockChatRoomItems(
  id: ChatRoomDTO["id"]
): Promise<ChatItemDTO[]> {
  await delay(Math.random() * MAX_DELAY_IDB);
  return [...CHAT_ROOMS.find((it) => it.id === id)!.items];
}

export async function sendMockChatMessage(
  id: ChatRoom["id"],
  body: LocalizedSingle<string>
): Promise<ChatItemDTO> {
  await delay(Math.random() * MAX_DELAY);
  if (Math.random() > 0.75) {
    throw new Error();
  }
  const msg = {
    id: genFakeUUID(),
    cDate: Date.now(),
    type: ChatItemType.MSG_OUTGOING,
    bodyOriginal: body,
    bodyHTML: body,
  };
  const idx = CHAT_ROOMS.findIndex((it) => it.id === id);
  const room = CHAT_ROOMS.splice(idx, 1)[0];
  CHAT_ROOMS.unshift(room);
  room.items.push(msg);
  room.latestAction = msg;
  return msg;
}

export async function getMockLikes(): Promise<LikeDTO[]> {
  await delay(Math.random() * MAX_DELAY);
  const now = eraseTimeOf(new Date());
  let countNew = genInt(5);
  return genArray(genInt(3, 7)).flatMap((dateOffset) => {
    const updated = addDays(now, -dateOffset);
    const updatedFormatted = `${updated.getFullYear()}-${updated.getMonth()}-${updated.getDate()}`;
    return genArray(genInt(dateOffset === 0 ? 2 : 12)).map(() => {
      const relation = genSwitch<Pick<LikeDTO, "inbound" | "outbound">>(
        () => ({ inbound: true, outbound: false }),
        () => ({ inbound: false, outbound: true }),
        () => ({ inbound: true, outbound: true })
      );
      return {
        ...relation,
        updated: updatedFormatted,
        new: relation.inbound && countNew-- > 0,
        profileSelf: sampleOne(USER.profiles)!,
        profileForeign: genProfileForeign(),
      };
    });
  });
}

function genProfileForeign(imgId = genInt(52, 128)): ProfileForeignSlim {
  return {
    age: genInt(18, 36),
    id: genFakeUUID(),
    name: genName(),
    picture: genSwitch(
      () => undefined,
      () => `https://placekitten.com/g/${imgId}/${imgId}`
    ),
  };
}

export async function getMockProfilePreviews(
  profile: ProfileSelf
): Promise<ProfilePreviewDTO[]> {
  await delay(Math.random() * MAX_DELAY);
  switch (profile.id) {
    case USER.profiles[0].id:
      return [
        {
          id: "profile-preview--0",
          name: "Jack Doe",
          age: 26,
          layout: ProfilePreviewLayout.SIMPLE,
          picture: "https://placekitten.com/g/200/300",
          attributes: [
            {
              label: {
                [Locale.EN_US]: "Size",
                [Locale.DE_DE]: "Größe",
              },
              value: {
                [Locale.EN_US]: '71"',
                [Locale.DE_DE]: "180 cm",
              },
            },
            {
              label: {
                [Locale.EN_US]: "Pronoun",
                [Locale.DE_DE]: "Pronomen",
              },
              value: {
                [Locale.EN_US]: "he/his",
                [Locale.DE_DE]: "er/sein",
              },
            },
          ],
          section: {
            label: {
              [Locale.EN_US]: "Which one is your favourite book?",
              [Locale.DE_DE]: "Welches ist Ihr Lieblingsbuch?",
            },
            value: {
              [Locale.EN_US]: genMessage(genInt(30, 90)),
            },
          },
        },
        {
          id: "profile-preview--1",
          name: "John",
          age: 28,
          layout: ProfilePreviewLayout.SIMPLE,
          attributes: [
            {
              label: {
                [Locale.EN_US]: "Size",
                [Locale.DE_DE]: "Größe",
              },
              value: {
                [Locale.EN_US]: '68"',
                [Locale.DE_DE]: "172 cm",
              },
            },
            {
              label: { [Locale.DE_DE]: "Lieblingsmensch" },
              value: { [Locale.DE_DE]: "Mama" },
            },
          ],
        },
        {
          id: "profile-preview--2",
          age: 27,
          layout: ProfilePreviewLayout.SIMPLE,
          attributes: [
            {
              label: {
                [Locale.EN_US]: "Haarfarbe",
                [Locale.DE_DE]: "Hair color",
              },
              value: {
                [Locale.EN_US]: "Blond",
                [Locale.DE_DE]: "Blonde",
              },
            },
          ],
          section: {
            label: {
              [Locale.EN_US]: "Describe yourself.",
              [Locale.DE_DE]: "Beschreiben Sie sich.",
            },
            value: { [Locale.DE_DE]: "Bin ganz okay" },
          },
        },
      ];
    case USER.profiles[1].id:
      return [];
    case USER.profiles[2].id:
      return [
        {
          id: "profile-preview--3",
          age: 32,
          layout: ProfilePreviewLayout.SIMPLE,
          picture: "https://placekitten.com/g/202/303",
          attributes: [
            {
              label: { [Locale.DE_DE]: "Spiel" },
              value: { [Locale.DE_DE]: "Das Schwarze Auge" },
            },
            {
              label: { [Locale.DE_DE]: "Klasse" },
              value: { [Locale.DE_DE]: "Schurke" },
            },
            {
              label: { [Locale.DE_DE]: "Magiebegabt" },
              value: { [Locale.DE_DE]: "Nein" },
            },
          ],
          section: {
            label: {
              [Locale.EN_US]: "Describe yourself.",
              [Locale.DE_DE]: "Beschreiben Sie sich.",
            },
            value: { [Locale.DE_DE]: "Ganz gemeiner Beutelschneider" },
          },
        },
      ];
    default:
      return [];
  }
}
