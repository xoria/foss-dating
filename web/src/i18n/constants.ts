import type { translations as EN_US } from "@/i18n/locales/en_US";

export type Translations = typeof EN_US;

export enum Locale {
  // order determines fallback search for missing translations
  EN_US = "en_US",
  DE_DE = "de_DE",
}

export const LOCALE_NAME: { [Key in Locale]: string } = {
  [Locale.EN_US]: "English",
  [Locale.DE_DE]: "Deutsch",
};

export const ISO_639_CODE: { [Key in Locale]: string } = {
  [Locale.EN_US]: "en",
  [Locale.DE_DE]: "de",
};

export const LOCALES = Object.values(Locale);
