import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    component: () => import("@/modules/landing/TheLandingView.vue"),
  },
  {
    path: "/",
    component: () => import("@/modules/frame/TheSimpleFrame.vue"),
    children: [
      {
        path: "/discover",
        component: () => import("./modules/discover/ThePeopleView.vue"),
      },
      {
        path: "/likes",
        component: () => import("./modules/likes/TheLikesView.vue"),
      },
    ],
  },
  {
    path: "/",
    component: () => import("@/modules/frame/TheContainedFrame.vue"),
    children: [
      {
        path: "/chat",
        component: () => import("./modules/chat/TheChatView.vue"),
      },
    ],
  },
  {
    path: "/",
    component: () => import("@/modules/frame/ThePanelFrame.vue"),
    children: [
      {
        path: "/profile",
        component: () => import("./modules/profile/TheProfileView.vue"),
      },
      {
        path: "/account",
        component: () => import("./modules/account/TheAccountView.vue"),
      },
      {
        path: "/support",
        component: () => import("./modules/static/TheSupportView.vue"),
      },
      {
        path: "/legal/imprint",
        component: () => import("./modules/static/legal/TheImprintView.vue"),
      },
      {
        path: "/legal/data-protection",
        component: () =>
          import("./modules/static/legal/TheDataProtectionView.vue"),
      },
      {
        path: "/logout",
        redirect: "/",
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
