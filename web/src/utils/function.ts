export function identity<T>(it: T): T {
  return it;
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
export function noop() {}
