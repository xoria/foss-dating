export function eraseTimeOf(datetime: Date): Date {
  const copy = new Date(datetime);
  copy.setHours(0);
  copy.setMinutes(0);
  copy.setSeconds(0);
  copy.setMilliseconds(0);
  return copy;
}

export function addDays(datetime: Date, days: number): Date {
  const clone = new Date(datetime);
  clone.setDate(clone.getDate() + days);
  return clone;
}
