import { defineComponent } from "vue";

export function dummyComponent<T>(factory: () => T) {
  return defineComponent({
    render() {
      return null;
    },
    setup() {
      return { data: factory() };
    },
  });
}
