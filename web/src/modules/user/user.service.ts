import { getMockUser } from "@/@mock/logged-in";
import { User } from "@/modules/user/user.types";

export async function fetchUser(): Promise<User> {
  return await getMockUser();
}
