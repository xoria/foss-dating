import {
  ProfilePreviewLayout,
  ProfilePreviewSimple,
} from "@/modules/profile/profile.service";
import { DefineComponent, PropType } from "vue";
import SimpleProfilePreview from "@/modules/profile/preview/SimpleProfilePreview.vue";

interface ProfilePreviewData {
  [ProfilePreviewLayout.SIMPLE]: ProfilePreviewSimple;
}

export const ProfilePreviewComponents: {
  [Key in ProfilePreviewLayout]: DefineComponent<
    {
      data: PropType<ProfilePreviewData[Key]>;
    },
    unknown,
    any // sadly, vue component types are still quite difficult to work with :-/
  >;
} = {
  [ProfilePreviewLayout.SIMPLE]: SimpleProfilePreview,
};
