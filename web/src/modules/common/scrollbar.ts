import { noop } from "@/utils/function";

const SCROLLING_HIGHLIGHT_DURATION = 125;
const SCROLLING_HIGHLIGHT_CLASSNAME = "scrolling";
const SCROLL_SILENT = Symbol("scrollbar::omit-scrolling");

let latestScrollCleanup: () => void = noop;

export function initScrollbar(element: Element) {
  let timeout: number | undefined;
  element.addEventListener(
    "scroll",
    () => {
      if (Reflect.get(element, SCROLL_SILENT)) {
        console.debug("omit scrolling", element);
        return;
      }
      window.clearTimeout(timeout);
      latestScrollCleanup();
      element.classList.add(SCROLLING_HIGHLIGHT_CLASSNAME);
      if (typeof WeakRef === "function") {
        const weakRef = new WeakRef(element);
        latestScrollCleanup = () =>
          weakRef?.deref()?.classList.remove(SCROLLING_HIGHLIGHT_CLASSNAME);
      } else {
        latestScrollCleanup = () =>
          element.classList.remove(SCROLLING_HIGHLIGHT_CLASSNAME);
      }
      timeout = window.setTimeout(() => {
        latestScrollCleanup();
        latestScrollCleanup = noop;
      }, SCROLLING_HIGHLIGHT_DURATION);
    },
    { passive: true }
  );
}

export function silentScrollContext(el: Element, handler: () => void) {
  Reflect.set(el, SCROLL_SILENT, true);
  handler();
  requestAnimationFrame(() => {
    Reflect.deleteProperty(el, SCROLL_SILENT);
  });
}
