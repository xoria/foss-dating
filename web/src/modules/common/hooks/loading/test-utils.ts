import {
  LoadingBusyVoid,
  LoadingError,
  LoadingSuccess,
} from "@/modules/common/hooks/loading/loading.types";
import { PromiseMayCancel } from "@/utils/promise";

export interface Trigger<T = number> {
  id: T;
  timeout: number;
  success: boolean;
  onCancel?: () => void;
}

export const LOADING_BUSY_VOID: LoadingBusyVoid = {
  loading: true,
  resolved: false,
  succeeded: false,
  predicted: false,
  value: null,
  error: null,
};

export function resolvedState<T>(
  trigger: Trigger<T>
): LoadingError<T> | LoadingSuccess<T> {
  return trigger.success
    ? {
        loading: false,
        resolved: true,
        succeeded: true,
        predicted: false,
        value: trigger.id,
        error: null,
      }
    : {
        loading: false,
        resolved: true,
        succeeded: false,
        predicted: false,
        value: null,
        error: trigger.id,
      };
}

export function createPromise<T>(data: Trigger<T>): PromiseMayCancel<T> {
  const promise = new Promise<T>((resolve, reject) =>
    setTimeout(
      () => (data.success ? resolve(data.id) : reject(data.id)),
      data.timeout
    )
  );
  if (data.onCancel) {
    return { promise, cancel: data.onCancel };
  }
  return promise;
}
