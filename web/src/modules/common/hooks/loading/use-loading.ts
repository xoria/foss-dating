import { Ref, watchEffect } from "vue";
import { castedRef } from "@/utils/reactive";
import { getPromise, isCancelable, PromiseMayCancel } from "@/utils/promise";
import { Loading } from "@/modules/common/hooks/loading/loading.types";

interface LoadingResult<T, E = never> {
  state: Ref<Loading<T, E>>;
  next: (req: PromiseMayCancel<T>, prediction?: T) => void;
  cancel: () => void;
}

export function useLoading<T, E = never>(
  initial?: PromiseMayCancel<T>
): LoadingResult<T, E> {
  let latestRequestId = -1;
  let state: Loading<T, E> = {
    loading: false,
    resolved: false,
    succeeded: false,
    predicted: false,
    value: null,
    error: null,
  };
  let pendingPromise: PromiseMayCancel<T> | undefined;

  const stateRef = castedRef<Loading<T, E>>(state);

  function next(promiseMayCancel: PromiseMayCancel<T>, prediction?: T) {
    cancel();
    pendingPromise = promiseMayCancel;
    const requestId = ++latestRequestId;

    if (prediction !== undefined) {
      stateRef.value = state = {
        loading: true,
        resolved: true,
        succeeded: true,
        predicted: true,
        value: prediction,
        error: null,
      };
    } else {
      stateRef.value = state = {
        ...state,
        loading: true,
        predicted: false,
      };
    }

    getPromise(promiseMayCancel)
      .then((value) => {
        if (requestId !== latestRequestId) {
          return;
        }
        stateRef.value = state = {
          loading: false,
          resolved: true,
          succeeded: true,
          predicted: false,
          value,
          error: null,
        };
        pendingPromise = undefined;
      })
      .catch((err) => {
        if (requestId !== latestRequestId) {
          return;
        }
        stateRef.value = state = {
          loading: false,
          resolved: true,
          succeeded: false,
          predicted: false,
          value: null,
          error: err as E,
        };
        pendingPromise = undefined;
      });
  }

  function cancel() {
    if (pendingPromise && isCancelable(pendingPromise)) {
      pendingPromise.cancel();
    }
  }

  if (initial) {
    next(initial);
  }

  return { state: stateRef, next, cancel };
}

export function useEffectLoading<T, E = never>(
  factory: () => PromiseMayCancel<T>,
  loading = useLoading<T, E>()
): LoadingResult<T, E>["state"] {
  watchEffect((onInvalidate) => {
    loading.next(factory());
    onInvalidate(loading.cancel);
  });
  return loading.state;
}
