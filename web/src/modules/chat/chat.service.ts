import {
  ProfileForeignSlim,
  ProfileSelf,
} from "@/modules/profile/profile.service";
import { LocalizedSingle } from "@/i18n/provider";
import {
  getMockChatRoomItems,
  getMockChatRooms,
  sendMockChatMessage,
} from "@/@mock/logged-in";

export enum ChatItemType {
  MSG_INCOMING = "MSG_IN",
  MSG_OUTGOING = "MSG_OUT",
}

interface ChatMessageCommonBase {
  id: string;
  type: ChatItemType;
  bodyHTML: LocalizedSingle<string>;
}
interface ChatMessageCommon extends ChatMessageCommonBase {
  cDate: Date;
}
interface ChatMessageCommonDTO extends ChatMessageCommonBase {
  cDate: number;
}

interface ChatMessageOutgoingBase extends ChatMessageCommonBase {
  type: ChatItemType.MSG_OUTGOING;
  bodyOriginal: LocalizedSingle<string>;
}
export type ChatMessageOutgoing = ChatMessageOutgoingBase & ChatMessageCommon;
export type ChatMessageOutgoingDTO = ChatMessageOutgoingBase &
  ChatMessageCommonDTO;
export type ChatMessageOutgoingPrediction = Omit<ChatMessageOutgoing, "id">;

interface ChatMessageIncomingBase extends ChatMessageCommonBase {
  type: ChatItemType.MSG_INCOMING;
}
type ChatMessageIncoming = ChatMessageIncomingBase & ChatMessageCommon;
export type ChatMessageIncomingDTO = ChatMessageIncomingBase &
  ChatMessageCommonDTO;

export type ChatItem =
  | ChatMessageOutgoing
  | ChatMessageIncoming
  | ChatMessageOutgoingPrediction;
export type ChatItemDTO = ChatMessageOutgoingDTO | ChatMessageIncomingDTO;

interface ChatRoomBase {
  id: string;
  profileSelf: ProfileSelf;
  profileForeign: ProfileForeignSlim;
}
export interface ChatRoom extends ChatRoomBase {
  latestAction?: ChatItem;
}
export interface ChatRoomDTO extends ChatRoomBase {
  latestAction?: ChatItemDTO;
}

export async function fetchChatRooms(): Promise<ChatRoom[]> {
  return (await getMockChatRooms()).map(chatRoomFromDTO);
}

export async function fetchChatRoomItems(
  id: ChatRoom["id"]
): Promise<ChatItem[]> {
  return (await getMockChatRoomItems(id)).map(chatItemFromDTO);
}

export async function sendChatMessage(
  id: ChatRoom["id"],
  body: LocalizedSingle<string>
): Promise<ChatItem> {
  return chatItemFromDTO(await sendMockChatMessage(id, body));
}

function chatRoomFromDTO(dto: ChatRoomDTO): ChatRoom {
  return {
    ...dto,
    latestAction: dto.latestAction && chatItemFromDTO(dto.latestAction),
  };
}

function chatItemFromDTO(dto: ChatItemDTO): ChatItem {
  switch (dto.type) {
    case ChatItemType.MSG_INCOMING:
    case ChatItemType.MSG_OUTGOING:
      return { ...dto, cDate: new Date(dto.cDate) };
  }
}
