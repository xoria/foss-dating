document.body.focus();

import "./styles/theme/index.scss";

import { createApp } from "vue";
import TheApp from "./modules/frame/TheApp.vue";
import router from "./router";
import { initScrollbar } from "@/modules/common/scrollbar";

initScrollbar(document.body);
createApp(TheApp).use(router).mount("body");
